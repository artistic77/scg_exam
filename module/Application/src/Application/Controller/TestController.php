<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;

use Zend\Http\Client;
use Zend\Http\Request;


/*
$this->params()->fromPost('paramname');   // From POST
$this->params()->fromQuery('paramname');  // From GET
$this->params()->fromRoute('paramname');  // From RouteMatch
$this->params()->fromHeader('paramname'); // From header
$this->params()->fromFiles('paramname');
*/
class TestController extends AbstractActionController
{
################################################################################ 
    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        
        //$this->adapter = new Adapter($this->config['Db']);
    }
################################################################################
    public function basic()
    {

        $view = new ViewModel();
        //Route
        $view->lang = $this->params()->fromRoute('lang', 'th');
        $view->action = $this->params()->fromRoute('action', 'test');
        $view->id = $this->params()->fromRoute('id', '');
        $view->page = $this->params()->fromQuery('page', 1);

        $view->x = $this->findX();
        return $view;       
    } 
################################################################################
    public function testAction() 
    {

        try
        {
            $view = $this->basic();
         
            return $view;
        }
        catch( Exception $e )
        {
            print_r($e);
        }
    }
################################################################################
  
    public function findX() {
        //3,5,9,15,X
        $num = array(3,5,9,15);
        for($i=1;$i<=5;$i++){
            $num[$i] = $num[$i-1]+2*($i-1);
            $result = $num[$i];
        }

        return $result;
    }
################################################################################

  /*  public function findLocation(){


        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";

        $location = '13.8234847,100.4906102';
        $rankby = 'distance';
        $radius = '1000';
        $type = 'restaurant';
        $key = 'AIzaSyDftxad9LxTbPyYBWmJYHL337DVvWA9ZD0';



        $data = array(
            "location" => $location,
            "rankby" => $rankby,
            "radius" => $radius, 
            "type" => $type, 
            "key" => $key
        );
        
        
        
        $query_url = sprintf("%s?%s", $url, http_build_query($data));
        echo $query_url."<br/>";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $query_url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_STREAM_WEIGHT, 256);
        curl_setopt($curl, CURLOPT_BUFFERSIZE, 128);

        
        $http_code = curl_getinfo($curl , CURLINFO_HTTP_CODE);
        $result = curl_exec($curl);
        
        //header('Content-type: application/json');
        echo $result;
        curl_close($curl);

    }*/

}


//$maps = new TestController();
//$maps->findLocation();