

function init()
{
    
}

function doNothing(){}




var app3 = new Vue({
  el: '#app3',
  created() {
      this.fetchData();	
  },
  data: {
      posts: []
  },
  methods: {
      fetchData() {
         /*
           axios.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.8234847,100.4906102&rankby=distance&type=restaurant&key=AIzaSyDftxad9LxTbPyYBWmJYHL337DVvWA9ZD0').then(response => {
          this.posts = response.data.results;
          });*/
          //I got the problem for about cross origin.So, I simulated the json result in text file
          axios.get('http://localhost:777/zf3-starter-kit/public/data_test.txt').then(response => {
            this.posts = response.data.results;
            });
      }
  }
});


// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.



// 1. Use plugin.
// This installs <router-view> and <router-link>,
// and injects $router and $route to all router-enabled child components
Vue.use(VueRouter)


// 2. Define route components
const Test = { template: '<div>Display Test Section</div>'}


// 3. Create the router
const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/zf3-starter-kit/public/test', component: Test }
  ]
})

// 4. Create and mount root instance.
// Make sure to inject the router.
// Route components will be rendered inside <router-view>.
new Vue({
  router,
  template: `
    <div id="app4">
      <h1>Test Router</h1>
      <ul>
        <router-link tag="li" to="/zf3-starter-kit/public/Test/" :event="['mousedown', 'touchstart']">
          <a>Test</a>
        </router-link>
      </ul>
      <router-view class="view"></router-view>
    </div>
  `
}).$mount('#app4')

// Find X
Vue.component('button-showresult',{

  data: function () {
    // `this` points to the vm instance
    var num = [3,5,9,15];
        for( var i=1; i<=5; i++){
            num[i] = num[i-1]+2*(i-1);
            result = num[i];
        }

        return result;
    //console.log('X is: ' + result)
  },  template: '<div id="app5">X is {{ result }} </div>'
})

new Vue({ el: '#components-demo' })

